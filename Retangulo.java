public class Retangulo extends OutrasFigurasGeometricas{


	double lado1;
	double lado2;
	double altura;
	double area;

	public double calcularArea(double base, double altura){
		area = base * altura;
		return area;
	}
}