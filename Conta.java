public class Conta {
	
	protected double saldo;
	protected String titular;
	protected String cpf;
	
	
	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void deposita(double valor) {
		this.saldo += valor;
		}
	
	public void saca(double valor) {
		this.saldo -= valor;
		}
	
		public double getSaldo() {
		return this.saldo;
		}
		
		void atualiza(double taxa) {
			this.saldo += this.saldo * taxa;
			}

		
		
}